export interface SmoothingOptions {
    periods: number,
    smoothingFactor?: number
}