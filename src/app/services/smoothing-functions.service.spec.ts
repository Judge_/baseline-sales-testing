import { TestBed } from '@angular/core/testing';
import { SmoothingOptions } from '../models';

import { SmoothingFunctionsService } from './smoothing-functions.service';

describe('SmoothingFunctionsService', () => {
  let service: SmoothingFunctionsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SmoothingFunctionsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#calculateMovingAverageSmoothing should return correct sample values', () => {
    const testInput: number[] = [50, 60, 40, 30, 90, -5, 40, 50, 30, 40];
    const testOptions: SmoothingOptions = { periods: 3 };

    expect(
      // Naughty hack to test private function
      service['calculateMovingAverageSmoothing'](testInput, testOptions)
    ).toEqual([
      50, 55, 50, 43.333333333333336, 53.333333333333336, 38.333333333333336,
      41.666666666666664, 28.333333333333332, 40, 40,
    ]);
  });

  it('#calculateSingleExponentialSmoothing should return correct Example 1 values', () => {
    const testInput: number[] = [50, 60, 40, 30, 90, -5, 40, 50, 30, 40];
    const testOptions: SmoothingOptions = { periods: 3, smoothingFactor: 0.4 };

    expect(
      // Naughty hack to test private function
      service['calculateSingleExponentialSmoothing'](testInput, testOptions)
    ).toEqual([
      50, 54, 48.4, 41.04, 60.623999999999995, 34.374399999999994, 36.62464,
      41.974784, 37.184870399999994, 38.31092224,
    ]);
  });

  it('#calculateSingleExponentialSmoothing should return correct Example 2 values', () => {
    const testInput: number[] = [15, 20, 16, 90, -90, -5, 17, 19, 16, 18];
    const testOptions: SmoothingOptions = { periods: 4, smoothingFactor: 0.5 };

    expect(
      // Naughty hack to test private function
      service['calculateSingleExponentialSmoothing'](testInput, testOptions)
    ).toEqual([
      35.25, 27.625, 21.8125, 55.90625, 0, 0, 8.5, 13.75, 14.875, 16.4375,
    ]);
  });
});
