# Baseline Sales

[![pipeline status](https://gitlab.com/Judge_/baseline-sales-testing/badges/main/pipeline.svg)](https://gitlab.com/Judge_/baseline-sales-testing/-/commits/main) [![coverage report](https://gitlab.com/Judge_/baseline-sales-testing/badges/main/coverage.svg)](https://gitlab.com/Judge_/baseline-sales-testing/-/commits/main)

This project is built with angular, you can access it publically at [https://judge\_.gitlab.io/baseline-sales-testing](https://judge_.gitlab.io/baseline-sales-testing)

### How to use

1. First download the example CSV files from _/src/assets/sample-data_
2. Select the required CSV file using the field on the app
3. Update the inputs to change the smoothing algorithms, you'll notice that you can only set the smoothing factor for the Single exponential algorithm
4. Press submit to analyse the file, you will notice the chart and export button now appear
5. Play around with the values until you get a result you like
6. Click _Export Baseline as CSV_ to download the new baseline values

### Testing

There are some automated tests for the 2 algorithms, to run them download the repo and install everything (Node, NPM, Angular, npm i) then run the _Test_ npm script. You can also see code coverage at the top of this document from the build.

### Builds

Builds, tests and linting is run for each commit, but changes are only deployed once merged to main.
