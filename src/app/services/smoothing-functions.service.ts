import { Injectable } from '@angular/core';
import { ParsedSalesInput, SmoothingOptions } from '../models';

export enum AlgorithmType {
  MovingAverage = 'moving-average',
  SingleExponential = 'single-exponential',
}

@Injectable({
  providedIn: 'root',
})
export class SmoothingFunctionsService {
  public processAndSmoothSales(
    csvData: ParsedSalesInput[],
    algorithmType: AlgorithmType,
    options: SmoothingOptions
  ): number[] {
    const salesValues: number[] = csvData.map((salesInput) => salesInput.Sales);
    const smoothedValues: number[] = [];

    switch (algorithmType) {
      case AlgorithmType.MovingAverage:
        smoothedValues.push(
          ...this.calculateMovingAverageSmoothing(salesValues, options)
        );
        break;

      case AlgorithmType.SingleExponential:
        smoothedValues.push(
          ...this.calculateSingleExponentialSmoothing(salesValues, options)
        );
        break;

      default:
        throw new Error('Incorrect Algorithm Type');
    }

    return smoothedValues;
  }

  private calculateMovingAverageSmoothing(
    salesData: number[],
    options: SmoothingOptions
  ): number[] {
    return salesData.map((sales, index) => {
      // Add one to avoid fencepost errors
      const periodIndex = index + 1;

      // We want the smaller of the period and index so we don't throw out the first few values
      const numberOfPeriodsToCheck = Math.min(periodIndex, options.periods);

      const periodsToCheck = salesData.slice(
        periodIndex - numberOfPeriodsToCheck,
        periodIndex
      );

      console.log(
        `Index ${index} (${sales}) has ${numberOfPeriodsToCheck} periods to check: ${periodsToCheck.toString()}`
      );

      return Math.max(this.getAverage(periodsToCheck), 0);
    });
  }

  private calculateSingleExponentialSmoothing(
    salesData: number[],
    options: SmoothingOptions
  ): number[] {
    const smoothingFactor = options.smoothingFactor ?? 0;
    const smoothedValues: number[] = [];

    salesData.forEach((sales, index) => {
      // We treat the first input differently because it doesn't have a previous value
      if (index === 0) {
        // We want the smaller of the period and index just incase it's a really small dataset
        const numberOfPeriodsToCheck = Math.min(
          salesData.length,
          options.periods
        );

        const periodsToCheck = salesData.slice(0, numberOfPeriodsToCheck);

        smoothedValues.push(Math.max(this.getAverage(periodsToCheck), 0));
      } else {
        smoothedValues.push(
          Math.max(
            sales * smoothingFactor +
              (1 - smoothingFactor) * smoothedValues[index - 1],
            0
          )
        );
      }
    });

    return smoothedValues;
  }

  private getAverage(numbersToAverage: number[]): number {
    return (
      numbersToAverage.reduce((acc, curr) => acc + curr) /
      numbersToAverage.length
    );
  }
}
